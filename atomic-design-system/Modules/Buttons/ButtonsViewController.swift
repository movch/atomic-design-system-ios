//
//  ButtonsViewController.swift
//  atomic-design-system
//
//  Created by Michail Ovchinnikov on 19/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

final class ButtonsViewController: RootViewController {
    override func loadView() {
        let view = BackgroundAtom()
        view.style = { ThemeProvider.current.theme.background }
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Buttons"

        let filledButton = ButtonMolecule()
        filledButton.background.style = { ThemeProvider.current.theme.roundedFilledButton }
        filledButton.label.text = "Filled"
        filledButton.label.style = { ThemeProvider.current.theme.filledButtonLabel }
        view.snap(filledButton, to: .xl, with: [
            pin(equal: \.safeAreaLayoutGuide.topAnchor, constant: Grid.xs.offset),
            pin(equal: \.heightAnchor, to: 44.0),
        ])

        let highlightedFilledButton = ButtonMolecule()
        highlightedFilledButton.isHighlighted = true
        highlightedFilledButton.background.style = { ThemeProvider.current.theme.roundedFilledButton }
        highlightedFilledButton.label.text = "Highlighted"
        highlightedFilledButton.label.style = { ThemeProvider.current.theme.filledButtonLabel }
        view.snap(highlightedFilledButton, to: .xl, with: [
            pin(equal: \.topAnchor, to: \.bottomAnchor, of: filledButton, constant: Grid.xs.offset),
            pin(equal: \.heightAnchor, to: 44.0),
        ])

        let disabledFilledButton = ButtonMolecule()
        disabledFilledButton.isEnabled = false
        disabledFilledButton.background.style = { ThemeProvider.current.theme.roundedFilledButton }
        disabledFilledButton.label.text = "Disabled"
        disabledFilledButton.label.style = { ThemeProvider.current.theme.filledButtonLabel }
        view.snap(disabledFilledButton, to: .xl, with: [
            pin(equal: \.topAnchor, to: \.bottomAnchor, of: highlightedFilledButton, constant: Grid.xs.offset),
            pin(equal: \.heightAnchor, to: 44.0),
        ])
    }
}
