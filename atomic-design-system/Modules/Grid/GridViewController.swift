//
//  GridViewController.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 12/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

final class GridViewController: UIViewController {
    private var labels = [UILabel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Grid"
        view.backgroundColor = .white

        Grid.allCases.forEach { grid in
            let label = UILabel()
            label.backgroundColor = .lightGray
            label.textAlignment = .center
            label.text = "\(String(describing: grid)) - \(grid.offset)px"

            var constraints = [pin(equal: \.safeAreaLayoutGuide.topAnchor)]
            if let lastLabel = labels.last {
                constraints = [
                    pin(equal: \.topAnchor, to: \.bottomAnchor, of: lastLabel, constant: Grid.xs.offset),
                ]
            }

            labels.append(label)

            self.view.snap(label, to: grid, with: constraints, safe: true)
        }
    }
}
