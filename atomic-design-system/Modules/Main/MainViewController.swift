//
//  MainViewController.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 12/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

final class MainViewController: RootViewController {
    private lazy var tableView = UITableView(frame: .zero, style: .plain)
    private lazy var tableAdapter = MainTableAdapter()
    private lazy var viewControllerFactory = ViewControllerFactory()

    override func loadView() {
        let view = BackgroundAtom()
        view.style = { ThemeProvider.current.theme.background }
        self.view = view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Design System Demo"

        setupTableView()
    }

    private func setupTableView() {
        tableAdapter.selectAction = { [weak self] menuItem in
            guard let self = self else { return }

            let controller = self.viewControllerFactory.controller(for: menuItem)
            self.showModule(controller)
        }

        tableView.dataSource = tableAdapter
        tableView.delegate = tableAdapter
        tableView.backgroundColor = .clear
        view.snapEdges(tableView)
    }
}

extension MainViewController: ModuleTransitionable {}
