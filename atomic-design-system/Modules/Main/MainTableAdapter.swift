//
//  MainTableAdapter.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 12/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

enum DemoMenuItem: CaseIterable {
    case grid
    case buttons
}

final class MainTableAdapter: NSObject {
    var selectAction: ((DemoMenuItem) -> Void)?
}

extension MainTableAdapter: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return DemoMenuItem.allCases.count
    }

    func tableView(_: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = String(describing: DemoMenuItem.allCases[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        return cell
    }
}

extension MainTableAdapter: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectAction = selectAction else {
            return
        }

        tableView.deselectRow(at: indexPath, animated: true)
        selectAction(DemoMenuItem.allCases[indexPath.row])
    }
}
