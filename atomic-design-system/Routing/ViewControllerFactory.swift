//
//  ViewControllerFactory.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 12/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

final class ViewControllerFactory {
    func controller(for item: DemoMenuItem) -> UIViewController {
        switch item {
        case .buttons:
            return ButtonsViewController()
        case .grid:
            return GridViewController()
        }
    }
}
