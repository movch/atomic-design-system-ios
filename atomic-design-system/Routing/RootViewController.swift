//
//  RootViewController.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 03/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let themeButton = UIBarButtonItem(title: "Theme", style: .plain, target: self, action: #selector(changeTheme))
        navigationItem.rightBarButtonItem = themeButton
    }

    @objc
    func changeTheme() {
        let themeId = UserDefaults.standard.integer(forKey: "themeId") == 1 ? 0 : 1
        UserDefaults.standard.set(themeId, forKey: "themeId")
        ThemeProvider.current.loadTheme()
    }
}
