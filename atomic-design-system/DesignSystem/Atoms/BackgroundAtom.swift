//
//  BackgroundAtom.swift
//  atomic-design-system
//
//  Created by Michail Ovchinnikov on 19/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

class BackgroundAtom: UIView {
    public var style: () -> Style<UIView> = { Style<UIView> { _, _ in } } {
        didSet {
            setNeedsLayout()
        }
    }

    private var state: UIControl.State = .normal
    private lazy var themeObserver = ThemeObserver()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        themeObserver.delegate = self
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        reloadStyle(for: state)
    }

    public func reloadStyle() {
        style().apply(to: self, state: state)
    }

    public func reloadStyle(for state: UIControl.State) {
        self.state = state
        reloadStyle()
    }
}

extension BackgroundAtom: ThemeObserverDelegate {
    func updateStyles() {
        setNeedsLayout()
    }
}
