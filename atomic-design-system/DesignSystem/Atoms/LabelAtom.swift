//
//  LabelAtom.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 27/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

class LabelAtom: UILabel {
    public var style: () -> Style<UILabel> = { Style<UILabel> { _, _ in } } {
        didSet {
            reloadStyle(for: state)
        }
    }

    private var state: UIControl.State = .normal
    private lazy var themeObserver = ThemeObserver()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        themeObserver.delegate = self
    }

    public func reloadStyle() {
        style().apply(to: self, state: state)
    }

    public func reloadStyle(for state: UIControl.State) {
        self.state = state
        reloadStyle()
    }
}

extension LabelAtom: ThemeObserverDelegate {
    func updateStyles() {
        reloadStyle(for: state)
    }
}
