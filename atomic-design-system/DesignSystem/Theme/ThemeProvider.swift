//
//  ThemeProvider.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 01/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import Foundation

final class ThemeProvider {
    static let current = ThemeProvider()

    var themes: [ThemeType] = [
        DefaultLightTheme(),
        DefaultDarkTheme(),
    ]

    var theme: ThemeType {
        if currentThemeIndex == nil {
            loadTheme()
        }

        return themes[currentThemeIndex!]
    }

    private var currentThemeIndex: Int?

    func loadTheme() {
        currentThemeIndex = UserDefaults.standard.integer(forKey: "themeId")
        NotificationCenter.default.post(name: .changeTheme, object: self)
    }
}
