//
//  DefaultDarkTheme.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 02/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

struct DefaultDarkTheme: ThemeType {
    let background = Style<UIView> { view, _ in
        view.backgroundColor = UIColor(named: "default-dark-background")
    }

    let roundedFilledButton = Style<UIView> { view, state in
        switch state {
        case .normal:
            view.backgroundColor = UIColor(named: "default-light-primary")
            break
        case .disabled:
            view.backgroundColor = UIColor(named: "default-light-primary")
            view.alpha = 0.3
            break
        default:
            view.backgroundColor = UIColor(named: "default-light-primary-dark")
        }
    }

    let filledButtonLabel = Style<UILabel> { label, state in
        label.textColor = .white
        label.textAlignment = .center

        if state == .disabled {
            label.textColor = .gray
        }
    }
}
