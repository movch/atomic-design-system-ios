//
//  DefaultLightTheme.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 02/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

struct DefaultLightTheme: ThemeType {
    let background = Style<UIView> { view, _ in
        view.backgroundColor = UIColor(named: "default-light-background")
    }

    let roundedFilledButton = Style<UIView> { view, state in
        switch state {
        case .normal:
            view.backgroundColor = UIColor(named: "default-light-primary")
            break
        case .disabled:
            view.backgroundColor = UIColor(named: "default-light-primary")
            view.alpha = 0.3
            break
        default:
            view.backgroundColor = UIColor(named: "default-light-primary-dark")
        }

        view.layer.cornerRadius = 5.0
    }

    let filledButtonLabel = Style<UILabel> { label, _ in
        label.textColor = .white
        label.textAlignment = .center
    }
}
