//
//  ThemeObserver.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 27/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let changeTheme = Notification.Name("changeDesignSystemTheme")
}

protocol ThemeObserverDelegate: class {
    func updateStyles()
}

final class ThemeObserver {
    weak var delegate: ThemeObserverDelegate?

    init() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didUpdateTheme),
                                               name: .changeTheme,
                                               object: nil)
    }

    @objc
    func didUpdateTheme() {
        delegate?.updateStyles()
    }
}
