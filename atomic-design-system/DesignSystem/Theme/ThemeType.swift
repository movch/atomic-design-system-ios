//
//  Themable.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 02/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

protocol ThemeType {
    var background: Style<UIView> { get }
    var roundedFilledButton: Style<UIView> { get }
    var filledButtonLabel: Style<UILabel> { get }
}
