//
//  ButtonMolecule.swift
//  atomic-design-system
//
//  Created by Michail Ovchinnikov on 19/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

class ButtonMolecule: UIButton {
    lazy var background = BackgroundAtom()
    lazy var label = LabelAtom()

    override var isHighlighted: Bool {
        didSet {
            background.reloadStyle(for: self.state)
            label.reloadStyle(for: self.state)
        }
    }

    override var isEnabled: Bool {
        didSet {
            background.reloadStyle(for: self.state)
            label.reloadStyle(for: self.state)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
        background.isUserInteractionEnabled = false
        snapEdges(background)

        label.isUserInteractionEnabled = false
        snap(label, to: .xs, with: [
            pin(equal: \.topAnchor),
            pin(equal: \.bottomAnchor),
        ])
    }
}
