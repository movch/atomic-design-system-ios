//
//  UIView+Grid.swift
//  atomic-design-system
//
//  Created by Michail Ovchinnikov on 11/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

/**
 Convininence methods to make it easier to use UI elements with the design system grid.
 */
public extension UIView {
    /// Helper method to position element in parent view with offsets, provided by grid.
    func snap(_ child: UIView, to grid: Grid = .sm, with constraints: [Constraint], safe: Bool = false) {
        var gridConstraints: [Constraint] = [
            pin(equal: \.leadingAnchor, constant: grid.offset),
            pin(equal: \.trailingAnchor, constant: -grid.offset),
        ]

        if safe {
            gridConstraints = [
                pin(equal: \.safeAreaLayoutGuide.leadingAnchor, constant: grid.offset),
                pin(equal: \.safeAreaLayoutGuide.trailingAnchor, constant: -grid.offset),
            ]
        }

        addSubview(child, constraints: gridConstraints + constraints)
    }

    /// Helper method to snap child view to the edges of parent view.
    func snapEdges(_ child: UIView, safe: Bool = false) {
        var gridConstraints: [Constraint] = [
            pin(equal: \.topAnchor),
            pin(equal: \.bottomAnchor),
        ]

        if safe {
            gridConstraints = [
                pin(equal: \.safeAreaLayoutGuide.topAnchor),
                pin(equal: \.safeAreaLayoutGuide.bottomAnchor),
            ]
        }

        snap(child, to: .zero, with: gridConstraints)
    }
}
