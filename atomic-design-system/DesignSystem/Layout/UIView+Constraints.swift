//
//  UIView+Constraints.swift
//  atomic-design-system
//
//  Created by Michail Ovchinnikov on 11/06/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

/**
 Solution is based on [Chris Eidof's Micro Auto Layout DSL](http://chris.eidhof.nl/post/micro-autolayout-dsl/) and [gist by adiki](https://gist.github.com/adiki/6cc4e3b16b3f933f7181c76bc6a52a8d)
 */

public typealias Constraint = (_ child: UIView, _ parent: UIView) -> NSLayoutConstraint

// MARK: Relations of view and parent view

func pin<Axis, Anchor>(equal keyPath: KeyPath<UIView, Anchor>, _ to: KeyPath<UIView, Anchor>, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { view, parent in
        let constraint = view[keyPath: keyPath].constraint(equalTo: parent[keyPath: to],
                                                           constant: constant)
        constraint.priority = priority
        return constraint
    }
}

func pin<Axis, Anchor>(greaterOrEqual keyPath: KeyPath<UIView, Anchor>, _ to: KeyPath<UIView, Anchor>, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { view, parent in
        let constraint = view[keyPath: keyPath].constraint(greaterThanOrEqualTo: parent[keyPath: to],
                                                           constant: constant)
        constraint.priority = priority
        return constraint
    }
}

func pin<Axis, Anchor>(lessOrEqual keyPath: KeyPath<UIView, Anchor>, _ to: KeyPath<UIView, Anchor>, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { view, parent in
        let constraint = view[keyPath: keyPath].constraint(lessThanOrEqualTo: parent[keyPath: to],
                                                           constant: constant)
        constraint.priority = priority
        return constraint
    }
}

// MARK: View layout dimension

func pin<LayoutDimension>(equal keyPath: KeyPath<UIView, LayoutDimension>, to constant: CGFloat) -> Constraint where LayoutDimension: NSLayoutDimension {
    return { view, _ in
        view[keyPath: keyPath].constraint(equalToConstant: constant)
    }
}

func pin<LayoutDimension>(equal keyPath: KeyPath<UIView, LayoutDimension>, to: KeyPath<UIView, LayoutDimension>, of view: UIView, constant: CGFloat = 0) -> Constraint where LayoutDimension: NSLayoutDimension {
    return { relatedView, _ in
        relatedView[keyPath: keyPath].constraint(equalTo: view[keyPath: to], constant: constant)
    }
}

func pin<LayoutDimension>(greaterOrEqual keyPath: KeyPath<UIView, LayoutDimension>, to constant: CGFloat) -> Constraint where LayoutDimension: NSLayoutDimension {
    return { view, _ in
        view[keyPath: keyPath].constraint(greaterThanOrEqualToConstant: constant)
    }
}

func pin<LayoutDimension>(lessThanOrEqual keyPath: KeyPath<UIView, LayoutDimension>, to constant: CGFloat) -> Constraint where LayoutDimension: NSLayoutDimension {
    return { view, _ in
        view[keyPath: keyPath].constraint(lessThanOrEqualToConstant: constant)
    }
}

// MARK: Relations of view and any other view

func pin<Anchor, Axis>(equal from: KeyPath<UIView, Anchor>, to: KeyPath<UIView, Anchor>, of view: UIView, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { relatedView, _ in
        let constraint = relatedView[keyPath: from].constraint(equalTo: view[keyPath: to],
                                                               constant: constant)
        constraint.priority = priority
        return constraint
    }
}

func pin<Anchor, Axis>(greaterOrEqual from: KeyPath<UIView, Anchor>, to: KeyPath<UIView, Anchor>, of view: UIView, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { relatedView, _ in
        let constraint = relatedView[keyPath: from].constraint(greaterThanOrEqualTo: view[keyPath: to],
                                                               constant: constant)
        constraint.priority = priority
        return constraint
    }
}

func pin<Anchor, Axis>(lessOrEqual from: KeyPath<UIView, Anchor>, to: KeyPath<UIView, Anchor>, of view: UIView, constant: CGFloat = 0, priority: UILayoutPriority = .required) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return { relatedView, _ in
        let constraint = relatedView[keyPath: from].constraint(lessThanOrEqualTo: view[keyPath: to],
                                                               constant: constant)
        constraint.priority = priority
        return constraint
    }
}

// MARK: Convinience functions

func pin<Axis, Anchor>(equal keyPath: KeyPath<UIView, Anchor>, constant: CGFloat = 0) -> Constraint where Anchor: NSLayoutAnchor<Axis> {
    return pin(equal: keyPath, keyPath, constant: constant)
}

public extension UIView {
    func addSubview(_ child: UIView, constraints: [Constraint]) {
        addSubview(child)
        child.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(constraints.map { $0(child, self) })
    }
}
