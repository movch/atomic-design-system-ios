//
//  Style.swift
//  atomic-design-system
//
//  Created by Michael Ovchinnikov on 02/07/2019.
//  Copyright © 2019 Michail Ovchinnikov. All rights reserved.
//

import UIKit

/**
 Main idea is based on [Composable, type-safe UIView styling with Swift functions](https://medium.cobeisfresh.com/composable-type-safe-uiview-styling-with-swift-functions-8be417da947f) article.
 */

struct Style<T: UIView> {
    let applyStyle: (T, UIControl.State) -> Void

    static func compose(_ styles: Style<T>...) -> Style<T> {
        return Style<T> { view, state in
            for style in styles {
                style.applyStyle(view, state)
            }
        }
    }

    func add(_ style: Style<T>) -> Style<T> {
        return Style { view, state in
            self.applyStyle(view, state)
            style.applyStyle(view, state)
        }
    }

    func apply(to view: T, state: UIControl.State = .normal) {
        applyStyle(view, state)
    }
}
